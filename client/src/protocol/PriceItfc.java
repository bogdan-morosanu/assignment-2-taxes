package protocol;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by moro on 11/23/16.
 */
public interface PriceItfc extends Remote {

    double getPrice(double pruchasePrice, int year) throws RemoteException;
}
