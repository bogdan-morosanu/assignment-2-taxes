package client;

/**
 * Created by moro on 11/23/16.
 */
public class Controller {

    private static View v;
    public static void main(String[] args) {

        v = new View();
        v.setVisible(true);

        v.addPriceActionLstn(new PriceListener(v));
        v.addTaxActionLstn(new TaxListener(v));
    }
}
