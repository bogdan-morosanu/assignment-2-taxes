package client;

import protocol.PriceItfc;
import protocol.TaxItfc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.Naming;

/**
 * Created by moro on 11/23/16.
 */
public class TaxListener implements ActionListener {

    private View cv;

    public TaxListener(View cv) {
        this.cv = cv;
    }

    public void actionPerformed(ActionEvent e) {
        TaxItfc tax;
        try {
            tax = (TaxItfc) Naming.lookup("rmi://localhost/Tax");

            double engineCap = cv.engineCap();
            double res = tax.getTax(engineCap);
            cv.updateTax(res);

        } catch (Exception ex) {
            cv.reset();
            System.err.println(e);
        }
    }
}


