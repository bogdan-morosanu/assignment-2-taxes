package client;

import protocol.PriceItfc;
import protocol.TaxItfc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.rmi.Naming;

/**
 * Created by moro on 11/23/16.
 */
public class PriceListener  implements ActionListener {

    private View cv;

    public PriceListener(View cv) {
        this.cv = cv;
    }

    public void actionPerformed(ActionEvent e) {
        PriceItfc price;
        try {
            price = (PriceItfc) Naming.lookup("rmi://localhost/Price");

            double pchsPrice = cv.pchsPrice();
            int year = cv.year();
            double res = price.getPrice(pchsPrice, year);
            cv.updatePrice(res);

        } catch (Exception ex) {
            cv.reset();
            System.err.println(e);
        }
    }
}

