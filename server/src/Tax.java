import protocol.TaxItfc;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by moro on 11/23/16.
 */
public class Tax extends UnicastRemoteObject implements TaxItfc {

    public Tax() throws RemoteException {

    }

    public double getTax(double engineCap) throws RemoteException {

        double aux = 8;

        aux = (engineCap > 1601) ? aux : 18;
        aux = (engineCap > 2001) ? aux : 72;
        aux = (engineCap > 2601) ? aux : 144;
        aux = (engineCap > 3001) ? aux : 290;

        return engineCap / 200.0 * aux;
    }
}
