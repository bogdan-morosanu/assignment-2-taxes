import protocol.PriceItfc;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * Created by moro on 11/23/16.
 */
public class Price extends UnicastRemoteObject implements PriceItfc {

    public Price() throws RemoteException {

    }
    public double getPrice(double pruchasePrice, int year) throws RemoteException {
        return pruchasePrice * (1.0 - 1.0 / 7.0 * (2015.0 - year));
    }
}
