
import java.rmi.Naming;

/**
 * Created by moro on 11/23/16.
 */
public class Server {

    public static void main(String[] args) {

        try {

            Tax tax = new Tax();
            Naming.rebind("rmi://localhost/Tax", tax);

            Price price = new Price();
            Naming.rebind("rmi://localhost/Price", price );

            System.out.println("Ready to serve.");

        } catch (Exception e) {
            System.err.println(e);
        }
    }
}
